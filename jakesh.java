/* 
 * Title: Project 1
 * Description: A Simple Linux (UNIX) Shell
 * Author: Jake Wasdin
 * Date due: 4 October 2014
 * 
 * Note: Tested on the following machine using OpenJDK 7. May not work on other Unices.
 * 
 * Linux jake-d520 3.13.0-24-generic #47-Ubuntu SMP Fri May 2 23:31:42 UTC 2014 i686 i686 i686 GNU/Linux
 * 
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;


public class jakesh {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	
	// used to keep track of console history
	static ArrayList<String> history = new ArrayList<String>();
	
	// used to keep track of current (working) directory
	static File currentDirectory;
	
	
	public static void main(String[] args) throws IOException, InterruptedException {
		
		
		while (true) {
		
		try {
			start();
		}
		
		// there is other error reporting, but at the least, do this so the console doesn't fail.
		catch (Exception e) {
			System.out.println("Error. Try a new command.");
		}
		
		}
		
		
	}
	
	// run terminal
	private static void start() throws IOException, InterruptedException {
		
		// initial run should set currentDirectory to home.
		if (currentDirectory == null) {
			currentDirectory = getHomeDirectory();
		}
		
		// get input
		Scanner scanner = new Scanner(System.in);
		
		prompt();
		
		
		while (true) {
			String input = scanner.nextLine();
			run(input);
			}
		
	}
	
	// run command: checks for a few locally implemented commands; if not found, passes to runCommand()
	private static void run(String command) throws IOException, InterruptedException {	
		// first, store command in history
		storeHistory(command);
		
		// history command
		if (command.equals("history")) {	
			printHistory();
			prompt();
		}
		
		// run last command...
		else if (command.equals("!!")) {
			run(getLastCommand());
		}
		
		// or run any other command in history
		else if (command.startsWith("!")) {
			int history = Integer.parseInt(command.replace("!",""));
			run(getCommand(history));
		}
		
		// cd command
		else if (command.startsWith("cd")) {
			cd(command);
		}
		
		// everything else!
		else {
			String[] commands = splitStringToArrayOfStrings(command);		
			runCommand(commands);
		}
	}
	
	// implements cd command. supports cd by itself (to go to home directory), cd [directory] with absolute and relative paths, and cd .. to go up one directory
	private static void cd(String command) throws IOException, InterruptedException {
		
		// note, in lab spec, user's home directory is identified as user.dir, but in current version of Java this refers to the current working directory. Using cd by itself to load the user's home directory. See: http://docs.oracle.com/javase/6/docs/api/java/lang/System.html#getProperties%28%29
		if (command.equals("cd")) {
			setWorkingDirectory(getHomeDirectory());
			run("pwd");
		}
		
		// now if not cd by itself
		else if (command.startsWith("cd")) {
			String args[] = splitStringToArrayOfStrings(command);
			
			// only 1 arg!
			if (args.length > 2) {
				System.out.println("Too many arguments. Use cd or cd directory or cd ..");
				prompt();
			}
			// support absolute and relative directory references
			else if (!args[1].startsWith("/")) {
				if (currentDirectory.toString().equals("/")) {
					args[1] = "/" + args[1];
				}
				else {
					args[1] = currentDirectory + "/" + args[1];}
				setWorkingDirectory(new File(args[1]));
				run("pwd");
			}
			else if (command.equals("cd ..")) {
				 setWorkingDirectory(new File(currentDirectory.toString().replace("[^/]*$", "")));
				 run("pwd");
			}
			else {
				setWorkingDirectory(new File(args[1]));
				run("pwd");
			}
		}
		
		
	}
	
	// used throughout
	private static String[] splitStringToArrayOfStrings(String str) {
		return str.split("\\s+");
	}
	
	// add one item to history
	private static void storeHistory(String command) {
		if (!command.equals("!!")) {
			history.add(command);}
	}
	
	// print out history
	private static void printHistory() {
		for(int i = 0; i < history.size(); i++) {
            System.out.println(i + ": " + history.get(i));
        }
	}
	
	// just to get last command
	private static String getLastCommand() {
		return history.get(history.size() -1);
	}
	
	// get command with int
	private static String getCommand(int i) {
		return history.get(i);
	}
	
	// used for running commands with process builder
	private static void runCommand(String commands[]) throws IOException, InterruptedException {

		// using process builder to have command and directory
		ProcessBuilder processor = new ProcessBuilder(commands);
		
		// make sure we're in a valid directory; otherwise go home.
		if (currentDirectory.exists()) {
			processor.directory(currentDirectory);}
		else {
			System.out.println("Could not find directory. Using home.");
			processor.directory(getHomeDirectory());
		}
		
		// start process
		Process process = processor.start();
		
		process.waitFor();

		// get input
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		
		String line;

		while ((line = reader.readLine()) != null) {
			System.out.println(line);
		}
		
		prompt();
		
		reader.close();
	}
	
	// used to get current home directory
	private static File getHomeDirectory() {
		// note, in lab spec, user's home directory is identified as user.dir, but in current version of Java this refers to the current working directory. Using cd by itself to load the user's home directory. See: http://docs.oracle.com/javase/6/docs/api/java/lang/System.html#getProperties%28%29
		File homeDirectory = new File(System.getProperty("user.home"));
		return homeDirectory;
	}
	
	// set current working directory
	private static void setWorkingDirectory(File directory) {
		currentDirectory = directory;
	}
	
	// used to print and include ">" for input
	private static void prompt(String message) {
	    System.out.println(message);
	    System.out.print("> ");
	}
	
	private static void prompt() {
	    System.out.print("> ");
	}

}
